<?php

//rutas del usuario
Auth::routes();
Route::get('/usuario/editar-informacion', 'HomeController@editUser')->name('edit-user');
Route::get('/api/all-category', 'TagController@apiAllTag');
Route::get('/api/all-sub-category/{id}', 'SubTagController@apiSubTag');
Route::get('/search/{query}', 'HomeController@search');
Route::get('/productos', 'HomeController@products')->name('products');
Route::get('/productos/{id}', 'HomeController@productsOfTag')->name('product-tag');
Route::put('/usuario/{user}', 'UserController@update')->name('user-update');

//rutas publicas 
Route::get('/quienes-somos', 'PageController@about')->name('about');
Route::get('/marca', 'PageController@marca')->name('marca');
Route::get('/como-ser-distribuidor', 'PageController@distributor')->name('distributor');
Route::get('/ayuda', 'PageController@help')->name('help');
Route::get('/contactanos', 'PageController@contact')->name('contact');


Route::get('/', 'HomeController@index')->name('home');
Route::get('/producto/{product}', 'HomeController@show')->name('home-show-product');
Route::post('/producto/agregar/{id}', 'HomeController@add' )->name('home-add-product');



//rutas publicas para el usuario
Route::group(['prefix' => 'ordenes-del-usuario'], function(){
  Route::get('/', 'OrderController@publicOrders')->name('order-public-index');
  Route::get('/pago/{order}', 'OrderController@payOrder')->name('pay-order');
  Route::put('/pago/{order}', 'OrderController@payment')->name('payment');
  Route::get('/{order}', 'OrderController@detail')->name('order-public-detail');
});


//rutas del administrador
Route::group(['prefix' => 'admin'], function () {

  //rutas para categorias
  Route::group(['prefix' => 'categorias'], function(){
    Route::get('/', 'TagController@index')->name('tag-index');
    Route::get('/{tag}/editar', 'TagController@edit')->name('tag-edit');
    Route::get('/nueva', 'TagController@create')->name('tag-new');
    Route::put('/{tag}', 'TagController@update')->name('tag-update');
    Route::post('/', 'TagController@store')->name('tag-create');
    Route::delete('/{tag}', 'TagController@destroy')->name('tag-delete');
  });

  //rutas para sub-categorias
  Route::group(['prefix' => 'sub-categorias'], function(){
    Route::get('/', 'SubTagController@index')->name('sub-tag-index');
    Route::get('/{subTag}/editar', 'SubTagController@edit')->name('sub-tag-edit');
    Route::get('/nueva', 'SubTagController@create')->name('sub-tag-new');
    Route::put('/{subTag}', 'SubTagController@update')->name('sub-tag-update');
    Route::post('/', 'SubTagController@store')->name('sub-tag-create');
    Route::delete('/{subTag}', 'SubTagController@destroy')->name('sub-tag-delete');
  });
  
  //rutas para productos
  Route::group(['prefix' => 'productos'], function () {
    Route::get('/', 'ProductController@index')->name('product-index');
    Route::get('/nuevo', 'ProductController@create')->name('product-new');
    Route::get('/{product}', 'ProductController@show')->name('product-show');
    Route::get('/{product}/editar', 'ProductController@edit')->name('product-edit');
    Route::post('/', 'ProductController@store')->name('product-create');
    Route::put('/{product}', 'ProductController@update')->name('product-update');
    Route::delete('/{product}', 'ProductController@destroy')->name('product-delete');
  });
  
  //rutas para ordenes
  Route::group(['prefix' => 'ordenes'], function () {
    Route::get('/', 'OrderController@index')->name('order-index');
    Route::get('/{order}', 'OrderController@show')->name('detail-order-admin');
    Route::get('/{order}/confirmar-pago', 'OrderController@edit')->name('confirm-payment');
    Route::post('/crear', 'OrderController@createOrder');
    Route::put('/{order}', 'OrderController@changeState')->name('order-changeState');
    Route::delete('/{order}', 'OrderController@destroy')->name('order-delete');        
  });
  
});
