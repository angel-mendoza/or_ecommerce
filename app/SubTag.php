<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubTag extends Model
{
    protected $fillable = [
        'name', 'tag_id'
    ];
    
    public function Product()
    {
        return $this->hasMany('App\Product');
    }

    public function tag()
    {
    	return $this->belongsTo('App\Tag');
    }
}
