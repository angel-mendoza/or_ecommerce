<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'tag_id', 'description', 'price', 'file', 'status', 'sub_tag_id' 
    ];    
    
    public function tag()
    {
    	return $this->belongsTo('App\Tag');
    }
    
    public function sub_tag()
    {
    	return $this->belongsTo('App\SubTag');
    }

    public function order_detail()
    {
        return $this->hasMany('App\OrderDetail');
    }       
}
