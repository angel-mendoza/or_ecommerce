<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name', 'icon'
    ];

    public function Product()
    {
        return $this->hasMany('App\Product');
    }

    public function sub_tag()
    {
        return $this->hasMany('App\SubTag');
    }
}
