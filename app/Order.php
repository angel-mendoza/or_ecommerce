<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'transaction', 'user_id', 'state', 'total' 
    ];    

    public function user()
    {
    	return $this->belongsTo('App\User');
    }  
    public function OrderDetail()
    {
    	return $this->hasMany('App\OrderDetail');
    }   
}
