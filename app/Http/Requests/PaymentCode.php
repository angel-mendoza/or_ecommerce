<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentCode extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transaction' => 'required|numeric'
        ];
    }
    public function messages()
    {
        return [
            'transaction.required' => 'El codigo es requerido',
            'transaction.numeric' => 'El codigo solo puede ingresar numeros',
        ];
    }     
}
