<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'description' => 'required|max:255',
            'category' => 'required',
            'sub-category' => 'required',
            'price' => 'required|numeric',
            'photo' => 'required|image'            
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'El Nombre es requerido',
            'name.max' => 'El Nombre no puede superar los 50 caracteres',
            'description.required' => 'El Nombre es requerido',
            'description.max' => 'El Nombre no puede superar los 255 caracteres',            
            'category.required' => 'La categoria es requerida',
            'sub-category.required' => 'La sub-categoria es requerida',
            'price.required' => 'El precio es requerido',
            'price.numeric' => 'Solo puede ingresar numeros',
            'photo.required' => 'La imagen del producto es requerida',
            'photo.image' => 'Solo puede cargar imagenes',
        ];
    }     
}
