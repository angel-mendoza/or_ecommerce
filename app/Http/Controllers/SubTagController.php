<?php

namespace App\Http\Controllers;

use DB;
use App\Tag;
use App\SubTag;
use Illuminate\Http\Request;
use App\Http\Requests\SubtagRequest;


class SubTagController extends Controller
{
    
    public function __construct()
    {
        //$this->middleware('auth', ['except' => ['apiAllTag']]);
        //$this->middleware('checkUserRole', ['except' => ['apiAllTag']]);
    }

    public function index()
    {
        $subTags = SubTag::all();
        return view('subTags.index', ['subTags' => $subTags])->with('alert', '');
    }

    public function create()
    {
        $tags = Tag::all();
        return view('subTags.new', ['tags' => $tags]);        
    }

    public function store(SubtagRequest $request)
    {
        SubTag::create([
            'name' => ucfirst($request['name']) ,
            'tag_id' => $request['category'] ,
        ]);
        return redirect()->route('sub-tag-index')->with('alert', 'Sub-categoria creada satifactoriamente.');
    }

    public function edit(SubTag $subTag)
    {
        $tags = Tag::all();
        return view('subTags.edit', ['subTag'=> $subTag, 'tags' => $tags ]);
    }


    public function update(SubtagRequest $request, SubTag $subTag)
    {
        $subTag->update([
            'name' => ucfirst($request['name']) ,
            'tag_id' => $request['category'] ,
        ]);
        return redirect()->route('sub-tag-index')->with('alert', 'Sub-categoria editada satifactoriamente.');
    }

    public function destroy(SubTag $subTag)
    {
        //DB::table('products')->where('tag_id', '=', $subTag->tag_id)->delete();
        $subTag->delete();
        return [true];
    }

    public function apiSubTag($id)
    {
        return DB::table('sub_tags')->where('tag_id', '=', $id)->get();
    }
}
