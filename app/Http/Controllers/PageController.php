<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function about()
    {
        return view('pages.about');
    }
    
    public function marca()
    {
        return view('pages.marca');
    }
    
    public function distributor()
    {
        return view('pages.distributor');
    }

    public function help()
    {
        return view('pages.help');
    }
    
    public function contact()
    {
        return view('pages.contact');
    }         
}

