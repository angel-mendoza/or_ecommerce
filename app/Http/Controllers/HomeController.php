<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\Product;
use App\Tag;
use App\SubTag;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = Product::paginate(8);
        return view('home', ['products' => $products])->with('alert', '');
    }
    
    public function show(Product $product)
    {
        return view('product.show', ['product'=>$product]);
    }
    
    public function add($id)
    {
        $user = Auth::user();
        $order = DB::table('orders')->where('user_id', '=', $user->id)->get();
        if (isset($order)) {
            $msg = "no hay orden";
        }else{
            $msg = "hay orden";
        }
        return [$id, $user->role, $order, $msg];
    }

    public function editUser()
    {   
        $user = Auth::user();
        return view('auth.edit', ['user'=>$user]);        
    }

    public function search($query)
    {
        $search = DB::table('products')
        ->where('name', 'like', '%'.$query.'%')
        ->get();
 
        return view('search', [
            'products'=>$search,
            'query'=> $query
        ]);
    }

    public function products()
    {
        $tags = Tag::all();
        return view('public-product.index', ['tags' => $tags])->with('alert', '');
    }
    
    public function productsOfTag($id)
    {
        $subTag = SubTag::find($id);
        $products = DB::table('products')->where('sub_tag_id', '=', $id)->get();
        if ($products->count() > 0) {
            # code...
            $title = $subTag->tag->name." - ".$subTag->name;
            return view('public-product.productsOfTag', ['products' => $products, 'title'=> $title])->with('alert', '');
        }else{
            return redirect()->route('products');
        }

    }
}
