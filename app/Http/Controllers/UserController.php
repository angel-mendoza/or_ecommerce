<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUser;

class UserController extends Controller
{
    public function update(UpdateUser $request, User $user)
    {
        $user->update([
            'name' => $request->name,
            'address' => $request->address,
            'dni' => $request->dni,
            'phone' => $request->phone,
        ]);
        return redirect()->route('home')->with('alert', 'Informacion personal editada satifactoriamente.');
    }
}
