<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\Order;
use App\OrderDetail;
use App\Http\Requests\PaymentCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkUserRole', ['except' => ['payment','createOrder','publicOrders', 'index', 'detail', 'payOrder', 'destroy']]); 
    }    
    public function index()
    {
        $user = auth()->user();
        if ($user->role == 'admin') {
            $orders = Order::all();
            return view('order.index', ['orders' => $orders])->with('alert', '');
        }else{
            return redirect()->route('order-public-index');
        }
    }
    
    public function publicOrders()
    {
        $id = auth()->user()->id;
        $orders = Order::where('user_id', $id)->get();
        return view('order.public', ['orders' => $orders])->with('alert', '');
    }

    public function payOrder(Order $order)
    {
        if($order->state == "transacción realizada" || $order->state == "pedido confirmado" || $order->state == "pedido cerrado" ){
            return redirect()->route('order-public-index');
        }
        return view('order.pay-order', ['order'=>$order]);
    }

    public function payment(PaymentCode $request, Order $order)
    {
        $order->update([
            'transaction' => $request['transaction'],
            'state' => "transacción realizada",
        ]);
        return redirect()->route('order-index')->with('alert', 'El codigo fue enviado satifactoriamente.');

    }


    public function detail(Order $order)
    {
        return view('order.detail', ['order'=>$order]);
    }


    public function show(Order $order)
    {
        return view('order.detail', ['order'=>$order]);
    }

    public function edit(Order $order)
    {
        return view('order.confirm-payment', ['order'=>$order]);        
    }

    public function changeState(Request $request, Order $order)
    {
        $validator = Validator::make($request->all(), [
            'state' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('confirm-payment', $order['id'])
                        ->withErrors($validator)
                        ->withInput();
        }
        $order->update([
            'state' => $request['state'],
        ]);
        return redirect()->route('order-index')->with('alert', 'El estado de la orden se cambio satifactoriamente.');
    }

    public function createOrder(Request $data)
    {
        $order = Order::create([
            'user_id' =>  auth()->user()->id,
            'total' => (double)$data['data']['total'],
        ]);
        
        foreach ($data['data']['carrito'] as $key => $value) {
            OrderDetail::create([
                'order_id' => $order->id,
                'product_id' => $value['id'],
                'amount'=> $value['amount']
            ]);
        }

        return [true];
    }

    public function destroy(Order $order)
    {
        DB::table('order_details')->where('order_id', '=', $order->id)->delete();
        $order->delete();
        return [true];
    }    
}

