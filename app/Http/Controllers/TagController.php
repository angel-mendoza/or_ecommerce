<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Requests\CreateTag;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['apiAllTag']]);
        $this->middleware('checkUserRole', ['except' => ['apiAllTag']]);
    }

    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $tags = Tag::all();
        return view('tag.index', ['tags' => $tags])->with('alert', '');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tag.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTag $request)
    {
        Tag::create([
            'name' => ucfirst($request['name']) ,
            'icon' => $request['icon'] ,
        ]);
        return redirect()->route('tag-index')->with('alert', 'Categoria creada satifactoriamente.');
    
    }

    public function edit(Tag $tag)
    {
        return view('tag.edit', ['tag'=> $tag]);
    }    

    public function update(CreateTag $request, Tag $tag)
    {
        $tag->update($request->all());
        return redirect()->route('tag-index')->with('alert', 'Categoria editada satifactoriamente.');
    }
    public function destroy(Tag $tag)
    {
        DB::table('products')->where('tag_id', '=', $tag->id)->delete();
        DB::table('sub_tags')->where('tag_id', '=', $tag->id)->delete();
        $tag->delete();
        return [true];
    }

    public function apiAllTag()
    {   
        return Tag::all();
    }    
}
