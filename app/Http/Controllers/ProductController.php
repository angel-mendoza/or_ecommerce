<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests\CreateProduct;
use App\Http\Requests\UpdateProduct;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkUserRole');
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('product.index', ['products' => $products])->with('alert', '');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return view('product.new', ['tags' => $tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProduct $request)
    {
        $filename = date('YmdGis');
        $file = $filename . '.' . $request->file('photo')->getClientOriginalExtension();
        $request->file('photo')->move('images/', $file);
        Product::create([
            'name' => ucfirst($request['name']) ,
            'tag_id' => $request['category'],
            'sub_tag_id' => $request['sub-category'],
            'description' => ucfirst($request['description']),
            'price' => (double)$request['price'],
            'file' => $file,
        ]);
        return redirect()->route('product-index')->with('alert', 'Producto creado satifactoriamente.');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('product.show', ['product'=>$product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $tags = Tag::all();
        return view('product.edit', ['product'=>$product, 'tags'=> $tags]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProduct $request, Product $product)
    {
        $filename = date('YmdGis');
        $file = $filename . '.' . $request->file('photo')->getClientOriginalExtension();
        $request->file('photo')->move('images/', $file);
        
        $product->update([
            'name' => ucfirst($request['name']) ,
            'tag_id' => $request['category'],
            'sub_tag_id' => $request['sub-category'],
            'description' => ucfirst($request['description']),
            'price' => (double)$request['price'],
            'file' => $file,
        ]);
        return redirect()->route('product-index')->with('alert', 'Producto editado satifactoriamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        DB::table('order_details')->where('product_id', '=', $product->id)->delete();
        $product->delete();
        return [true];
    }
}
