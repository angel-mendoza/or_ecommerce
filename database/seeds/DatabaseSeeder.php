<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $users = factory(App\User::class)->times(20)->create();
      // $tags = factory(App\Tag::class)->times(30)->create();

      // $tags->each(function(App\Tag $tag){
      //   factory(App\Product::class)->create([
      //     'tag_id' => $tag->id,
      //     //'tag_id' => $tags->random(1)->first()->id,
      //   ]);
      // });


      DB::table('users')->insert([
        'name' =>'Angel Mendoza',
        'email' =>'test@gmail.com',
        'dni' => '20819315',
        'address' => 'Algun lugar de machala',
        'password' => bcrypt('123'),
        'role'=> 'admin',
        'phone'=> '0987776655',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('users')->insert([
        'name' =>'Eiley vargas',
        'email' =>'cliente@gmail.com',
        'dni' => '20819315',
        'address' => 'Algun lugar de machala',
        'password' => bcrypt('123'),
        'role'=> 'user',
        'phone'=> '0987776655',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

    }
}
