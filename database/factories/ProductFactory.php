<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->company,        
        'description' => $faker->text($maxNbChars = 100),        
        'price' => $faker->numberBetween($min = 5, $max = 100),
        'file' => $faker->imageUrl(800, 600, 'people')        
    ];
});
