const navLinkActive = ( id )=>{
  $( '.nav-item' ).removeClass( "active" )
  $( id ).addClass( "active" );
}

const clearLink = () =>{
  $( '.nav-item' ).removeClass( "active" )
}

  const consoleAlert = ()=>{
    console.log("%c¡Detente!", "font-family: ';Arial';, serif; font-weight: bold; color: red; font-size: 45px");
    console.log("%cEsta función del navegador está pensada para desarrolladores. Si alguien te indicó que copiaras y pegaras algo aquí para habilitar una funcion o para PIRATEAR la cuenta de alguien, se trata de un fraude.", "font-family: ';Arial';, serif; color: black; font-size: 20px");
    console.log("%cSi lo haces, esta persona podrá acceder a tu cuenta y datos personales.", "font-family: ';Arial';, serif; color: black; font-size: 20px");
  }

const changeTitle = (id) => {
  $("#titulo").html(`Laravel - ${id}`)
} 

const initializeDataTable = () => {
  $('#dataTable').DataTable()
}

const initializeselect2 = () => {
  $(".select2").select2();    
}

const deletedSA = (id, rute, element, msg) => {
  let token = $('#csrf-token').attr('content');
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger mr-3'
    },
    buttonsStyling: false
  })
  swalWithBootstrapButtons.fire({
    title: 'Esta seguro?',
    text: msg ? msg  : "se borrara los datos de manera permanente!" ,
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Si, eliminar!',
    cancelButtonText: 'No, cancelar!',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {

      $.ajax({
      
        url: `/admin/${rute}` ,
        type: 'POST',
        dataType: "JSON",
        data: {
            "id": id,
            "_method": 'DELETE',
            "_token": token,
        },        
        success: (data) => {
          swalWithBootstrapButtons.fire(
            'Eliminada!',
            `${element} se elimino satifactoriamente`,
            'success'
          ).then((res) =>{
            if(res.value){
              location.reload()
            }else if (result.dismiss === Swal.DismissReason.cancel) {
              location.reload()
            }
          })
        },
        error: ()=>{
          swalWithBootstrapButtons.fire("Ocurrio un error", "vuelva a intentarlo mas tarde", "error")
        }

      })
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons.fire(
        'Cancelado',
        'Los datos se encuentran a salvo :)',
        'error'
      )
    }
  })
}

const addProduct = (id, rute) => {
  let token = $('#csrf-token').attr('content');
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger mr-3'
    },
    buttonsStyling: false
  })
  $.ajax({
    url: rute,
    type: 'POST',
    dataType: "JSON",
    data: {
        "id": id,
        "_method": 'POST',
        "_token": token,
    },        
    success: (data) => {
      swalWithBootstrapButtons.fire(
        'Agregado!',
        `El producto se agrego satifactoriamente`,
        'success'
      ).then((res) =>{
        if(res.value){
          location.reload()
        }else if (result.dismiss === Swal.DismissReason.cancel) {
          location.reload()
        }
      })
    },
    error: ()=>{
      swal("Ocurrio un error", "vuelva a intentarlo mas tarde", "error")
    }

  })
}

const badge = (num) => {
  if(num < 1){
    num = ""
  }
  document.getElementById("amount-total").innerHTML = num
} 

const fillModalShopping = () => {
  let carrito = JSON.parse(sessionStorage.getItem('carrito'))
  let total = sessionStorage.getItem('total')
  $("#body-shopping-cart").html("")
  carrito.forEach(element => {
      //$("#body-shopping-cart").append(`<tr><th><button class="btn btn-outline-danger btn-sm deleted-item" data-id="${element.id}" data-toggle="tooltip" data-placement="bottom" title="Eliminar producto"><i class="far fa-trash-alt"></i></button></th><td>${element.title}</td><td>${element.amount}</td><td>${element.price}$</td><td>${element.total}$</td></tr>`)
      $("#body-shopping-cart").append(`
        <tr>
          <td>
            <button class="btn btn-outline-danger deleted-item" data-id="${element.id}" data-toggle="tooltip" data-placement="bottom" title="Eliminar producto"><i class="far fa-trash-alt"></i></button>
          </td>
          <td class="pt-3">${element.title}</td>
          <td>
            <span data-toggle="tooltip" data-placement="bottom" title="Cantidad del producto">
              <input type="number" data-id="${element.id}" class="form-control input-cart-amount"  value="${element.amount}">                                    
            </span>
          </td>
          <td class="pt-3">${element.price}$</td>
          <td class="pt-3">${element.total}$</td>
        </tr>`
      )
    })
    $("#body-shopping-cart").append(`<tr><th colspan="3"></th><th>Total a pagar:</th><th>${total}$</th></tr>`)
}

const clearModalShopping = () => {
  $("#body-shopping-cart").html("")
}

const fillSelectSubTag = (selector,category) => {
  if(category == ''){
    $(`${selector} option`).remove();
    $(selector).append('<option value="" >Elija una categoria primero</option>')
  }else{
    $.ajax({
      type: 'GET',
      url: `/api/all-sub-category/${category}`,
      dataType: 'json',
      success: (data)=>{
        $(`${selector} option`).remove();
        $(selector).append('<option value="0" >Elija una Sub-categoria</option>')
          $.each(data,(key, subTag)=> {
              $(selector).append(`<option value="${subTag.id}" >${subTag.name}</option>`)
          })
      },
      error: ()=>{
        $(`${selector} option`).remove();
        $(selector).append('<option value="0" >Ocurrio un error, recargue el navegador</option>')
      }
    })    
  }
}


export default {fillSelectSubTag , fillModalShopping, clearModalShopping,  addProduct, clearLink, navLinkActive, consoleAlert, changeTitle, initializeDataTable, initializeselect2 , deletedSA, badge}