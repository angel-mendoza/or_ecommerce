import  appFn from './index'

const add = (carrito) => {
  let storage = sessionStorage.getItem('carrito')
  if(carrito.amount > 0 ){
    if(storage){
      //hay carrito
      let compra = JSON.parse(  sessionStorage.getItem('carrito') );
      let nuevo = []
      let newTotal = 0
      sessionStorage.removeItem('carrito')
      sessionStorage.removeItem('total')            

      compra.forEach(element => {
        if(element.id == carrito.id){
          carrito.total = element.total + carrito.total
          carrito.amount = element.amount + carrito.amount
        }else{
          nuevo.push(element)
        }
      })
      nuevo.push(carrito)
      nuevo.forEach(element => {
        newTotal = newTotal + element.total
      });
      sessionStorage.setItem('carrito',JSON.stringify(nuevo))
      sessionStorage.setItem('total', newTotal)
      let productosTotal =  JSON.parse(sessionStorage.getItem('carrito'))
      appFn.badge(productosTotal.length)
      
      Swal.fire({
        type: 'success',
        title: 'Producto agregado al carrito',
        timer: 3000
      })    
      appFn.fillModalShopping()    
      
    }else{
      //no hay carrito
      let compra = []
      compra.push(carrito)
      sessionStorage.setItem('carrito',JSON.stringify(compra))
      sessionStorage.setItem('total',carrito.total)
      let productosTotal =  JSON.parse(sessionStorage.getItem('carrito'))
      appFn.badge(productosTotal.length)
      Swal.fire({
        type: 'success',
        title: 'Producto agregado al carrito',
        timer: 3000
      })   
      appFn.fillModalShopping()    
    }
  }else{
    Swal.fire(
      'Error',
      'no puedes agregar productos si su cantidad es inferior o igual a 0',
      'error'
    )
  }  

}

const deleteOrder = () => {
  $(document).on('click', 'button#delete-order',()=>{

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger mr-3'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Esta seguro?',
      text: "su historial de pedidos se borrara de manera permanente",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, borrar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        swalWithBootstrapButtons.fire(
          'Borrados!',
          'Su orden fue eliminada.',
          'success'
        )
        appFn.badge("")
        appFn.clearModalShopping()
        sessionStorage.removeItem('carrito')
        sessionStorage.removeItem('total')
        $('#modal-shopping-cart').modal('hide')
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'Tu orden se encuentra a salvo :)',
          'error'
        )
      }
    })

  })  
}

const processOsrder = () => {
  let token = $('#csrf-token').attr('content');
  let carrito =  JSON.parse(sessionStorage.getItem('carrito')) 
  let total =  sessionStorage.getItem('total') 
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger mr-3'
    },
    buttonsStyling: false
  })
  
  swalWithBootstrapButtons.fire({
    title: 'Esta seguro?',
    text: "esta a punto de tramitar la orden, despues de creada no se puede editar.",
    type: 'info',
    showCancelButton: true,
    confirmButtonText: 'Si, procesar',
    cancelButtonText: 'No, cancelar!',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      $("#table-product").hide()
      $("#loading-body").show()
      $("#process-order").attr("disabled", true)
      $("#delete-order").attr("disabled", true)
 
      $.ajax({
        url: '/admin/ordenes/crear',
        type: 'POST',
        dataType: "JSON",
        data: {
            "data": { carrito, total },
            "_method": 'POST',
            "_token": token,
        },        
        success: (data) => {
          swalWithBootstrapButtons.fire(
            'Orden Procesada!',
            `su orden fue recivida satisfactoriamente`,
            'success'
          ).then((res) =>{
            if(res.value){
              $("#process-order").attr("disabled", false)
              $("#delete-order").attr("disabled", false)
              appFn.badge("")
              appFn.clearModalShopping()
              sessionStorage.removeItem('carrito')
              sessionStorage.removeItem('total')
              $('#modal-shopping-cart').modal('hide')
              $("#table-product").show()
              $("#loading-body").hide()
            }else if (result.dismiss === Swal.DismissReason.cancel) {
              $("#process-order").attr("disabled", false)
              $("#delete-order").attr("disabled", false)
              appFn.badge("")
              appFn.clearModalShopping()
              sessionStorage.removeItem('carrito')
              sessionStorage.removeItem('total')
              $('#modal-shopping-cart').modal('hide')
              $("#table-product").show()
              $("#loading-body").hide()
            }
          })
        },
        error: (err)=>{
          $("#process-order").attr("disabled", false)
          $("#delete-order").attr("disabled", false)
          $("#table-product").show()
          $("#loading-body").hide()          
          swalWithBootstrapButtons.fire("Ocurrio un error", "vuelva a intentarlo mas tarde", "error")
        }
    
      })
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons.fire("Ocurrio un error", "vuelva a intentarlo mas tarde", "error")
    }
  })
}

const deleteItem = (id) => {

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger mr-3'
    },
    buttonsStyling: false
  })
  
  swalWithBootstrapButtons.fire({
    title: 'Esta seguro?',
    text: "el producto se borrara de su orden",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Si, borrar!',
    cancelButtonText: 'No, cancelar!',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      swalWithBootstrapButtons.fire(
        'Borrados!',
        'Su orden fue eliminada.',
        'success'
      )
      let carrito = JSON.parse(  sessionStorage.getItem('carrito') );
      let nuevo = []
      let newTotal = 0
      sessionStorage.removeItem('carrito')
      sessionStorage.removeItem('total')   
    
      carrito.forEach(element => {
        if(element.id != id){
          newTotal = newTotal + element.total
          nuevo.push(element)
        }
      })
    
      sessionStorage.setItem('carrito',JSON.stringify(nuevo))
      sessionStorage.setItem('total', newTotal)
      let newcarrito = JSON.parse(  sessionStorage.getItem('carrito') );
      appFn.badge(newcarrito.length)
      appFn.fillModalShopping()
      if(newcarrito.length < 1){
        $('#modal-shopping-cart').modal('hide')
      }
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons.fire(
        'Cancelado',
        'Tu orden se encuentra a salvo :)',
        'error'
      )
    }
  })
}

const changeAmountCart = (id, value) => {
  let carrito = JSON.parse(  sessionStorage.getItem('carrito') );
  let nuevo = []
  let newTotal = 0
  sessionStorage.removeItem('carrito')
  sessionStorage.removeItem('total')   

  if(value < 1){
    itemCero(id)
  }
  
  carrito.forEach(element => {
    if(element.id == id){
      element.amount = value
      element.total = element.price * value
    }
    newTotal = newTotal + element.total
    nuevo.push(element)
  })

  sessionStorage.setItem('carrito',JSON.stringify(nuevo))
  sessionStorage.setItem('total', newTotal)
  let newcarrito = JSON.parse(  sessionStorage.getItem('carrito') );
  appFn.badge(newcarrito.length)
  appFn.fillModalShopping()
  if(newcarrito.length < 1){
    $('#modal-shopping-cart').modal('hide')
  }
}

const itemCero = (id)=>{
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger mr-3'
    },
    buttonsStyling: false
  })
  
  swalWithBootstrapButtons.fire({
    title: 'Esta seguro?',
    text: "el producto se borrara de su orden",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Si, borrar!',
    cancelButtonText: 'No, cancelar!',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      swalWithBootstrapButtons.fire(
        'Borrados!',
        'Su orden fue eliminada.',
        'success'
      )
      let carrito = JSON.parse(  sessionStorage.getItem('carrito') );
      let nuevo = []
      let newTotal = 0
      sessionStorage.removeItem('carrito')
      sessionStorage.removeItem('total')   
    
      carrito.forEach(element => {
        if(element.id != id){
          newTotal = newTotal + element.total
          nuevo.push(element)
        }
      })
    
      sessionStorage.setItem('carrito',JSON.stringify(nuevo))
      sessionStorage.setItem('total', newTotal)

      let newcarrito = JSON.parse(  sessionStorage.getItem('carrito') );
      appFn.badge(newcarrito.length)
      appFn.fillModalShopping()

      if(newcarrito.length < 1){
        $('#modal-shopping-cart').modal('hide')
      }
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {

      let carrito = JSON.parse(sessionStorage.getItem('carrito'));
      let nuevo = []
      let newTotal = 0

      sessionStorage.removeItem('carrito')
      sessionStorage.removeItem('total')   
    
      carrito.forEach(element => {
        if(element.id == id){
          element.amount = 1
          element.total = element.price * element.amount
        }
        newTotal = newTotal + element.total
        nuevo.push(element)
      })

      sessionStorage.setItem('carrito',JSON.stringify(nuevo))
      sessionStorage.setItem('total', newTotal)

      appFn.badge(carrito.length)
      appFn.fillModalShopping()

      swalWithBootstrapButtons.fire(
        'Cancelado',
        'Tu orden se encuentra a salvo :)',
        'error'
      )
    }
  })
}

export default {add , deleteOrder, processOsrder, deleteItem, changeAmountCart, itemCero}