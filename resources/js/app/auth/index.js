import  appFn from './../../funtions'

class LoginIndex {
    constructor() {
      //valores por defectos
      appFn.navLinkActive('#login')
      appFn.consoleAlert()
    } 
}

class RegisterIndex {
  constructor(){
    appFn.navLinkActive('#register')
    appFn.consoleAlert()
  }
}

class EditUser {
  constructor(){
    appFn.consoleAlert()
  }
}

window.EditUser = EditUser
window.LoginIndex = LoginIndex
window.RegisterIndex = RegisterIndex