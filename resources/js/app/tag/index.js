import  appFn from './../../funtions'

class TagIndex {
  constructor(){
    appFn.consoleAlert()
    appFn.initializeDataTable()
    this.event()
  }
  event(){
    $(document).on('click', 'button.deleted',(e)=>{
      let idSlected = $(e.currentTarget).data('id')
      let url = `categorias/${idSlected}/`
      let msg = 'Al borrar esta categoria se borrara las sub-categarias y los productos que posean dicha categoria.'
      let element = 'La Categoria'
      appFn.deletedSA(idSlected, url, element, msg)
    })
  }    
}

class TagNew {
  constructor(){
    appFn.consoleAlert()
    //appFn.initializeselect2()        
  }
}

class TagEdit{
  constructor(){
    appFn.consoleAlert()
  }
}
window.TagIndex = TagIndex
window.TagNew = TagNew
window.TagEdit = TagEdit