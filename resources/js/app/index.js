import  appFn  from '../funtions'
import shopping from '../funtions/shopping'

sessionStorage.setItem('search', 'all')

//accion para abrir o no la modal del carrito de compra
$(document).on('click', '.link-cart',(e)=>{
    let carrito = JSON.parse(sessionStorage.getItem('carrito'))
    if(carrito){
        if(carrito.length > 0){
            $('#modal-shopping-cart').modal('show')
        }else{
            Swal.fire({
                title: 'Aun no has agregado ningun producto al carrito.',
                type: 'warning'
            })    
        }
    }else{
        Swal.fire({
            title: 'Aun no has agregado ningun producto al carrito.',
            type: 'warning'
        })
    }

})

//accion para eliminar la orden
$(document).on('click', 'button#delete-order',()=>{
    shopping.deleteOrder()    
})

//accion para procesar la orden
$(document).on('click', 'button#process-order',()=>{
    shopping.processOsrder()   
})

//accion para cambiar la cantidad de un producto en el carrito
$(document).on('change', '.input-cart-amount',(e)=>{
    let idSlected = $(e.currentTarget).data('id')
    let value = $(e.currentTarget).val()
    let input = $(e.currentTarget)
/*     if(value < 1){
        shopping.itemCero(idSlected)
    } */
    shopping.changeAmountCart(idSlected,value)
})

//accion para eliminar un producto del carrito de compra 
$(document).on('click', 'button.deleted-item',(e)=>{
    let idSlected = $(e.currentTarget).data('id')
    shopping.deleteItem(idSlected)
})

//esto inicializa el badge de la cantidad de productos en el icono del carrito
let carrito = JSON.parse(sessionStorage.getItem('carrito'))
if(carrito){
    appFn.badge(carrito.length)
    appFn.fillModalShopping()
}

//cambia el nombre del boton de categorias 
// $(document).on('click' , 'a.category-link', (e)=>{
//     let tag = $(e.currentTarget)
//     $('.btn-all-tag').text(tag.text())
//     sessionStorage.removeItem('search')
//     sessionStorage.setItem('search', tag.data('id'))
// })

//buscar producto
$(document).on('click', '#btn-search-product', ()=>{
    let query = $('#input-search-product').val()
    if(query == ''){
        Swal.fire({
            title: 'Cuidado.',
            text: 'el campo de busqueda esta vacio.',
            type: 'info'
        })            
    }else{
        let home = window.location.origin
        window.location.replace(`${home}/search/${query}`)

    }
})
//cargar las categorias
/* $(document).ready(()=>{
    $.ajax({
        type: 'GET',
        url: '/api/all-category',
        dataType: 'json',
        success: (data)=>{
            $('.menu-all-tag').append(`<a data-id="all" class="dropdown-item category-link">Todas las categorias</a>`)
            $.each(data,(key, state)=> {
                $('.menu-all-tag').append(`<a data-id="${state.id}" class="dropdown-item category-link">${state.name}</a>`)
            })
        },
        error: (data)=>{
            Swal.fire("Ocurrio un error", "Ocurrio un error cargando las categorias, vuelva a intentarlo mas tarde", "error")
        }
    })
    
}) */
