import  appFn from './../../funtions'

class SubTagIndex {
  constructor(){
    appFn.consoleAlert()
    appFn.initializeDataTable()
    this.event()
  }
  event(){
    $(document).on('click', 'button.deleted',(e)=>{
      let idSlected = $(e.currentTarget).data('id')
      let url = `sub-categorias/${idSlected}/`
      let msg = 'Al borrar esta sub-categoria se borrara tambien todos los productos que posean dicha sub-categoria.'
      let element = 'La Sub-Categoria'
      appFn.deletedSA(idSlected, url, element, msg)
    })
  }    
}

class SubTagNew {
    constructor(){
      appFn.consoleAlert()
      appFn.initializeselect2()        
    }
}
/* 
class TagEdit{
  constructor(){
    appFn.consoleAlert()
  }
}
window.TagNew = TagNew
window.TagEdit = TagEdit */
window.SubTagIndex = SubTagIndex
window.SubTagNew = SubTagNew