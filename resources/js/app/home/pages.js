import  appFn from './../../funtions'

class PageProduct {
    constructor() {
      //valores por defectos
      appFn.navLinkActive('#products')
      appFn.consoleAlert()
    } 
}

class PageAbout {
    constructor() {
      //valores por defectos
      appFn.navLinkActive('#about')
      appFn.consoleAlert()
    } 
}

class PageMarca {
    constructor() {
      //valores por defectos
      appFn.navLinkActive('#marca')
      appFn.consoleAlert()
    } 
} 

class PageDistributor {
    constructor() {
      //valores por defectos
      appFn.navLinkActive('#distributor')
      appFn.consoleAlert()
    } 
} 

class PageHelp {
    constructor() {
      //valores por defectos
      appFn.navLinkActive('#help')
      appFn.consoleAlert()
    } 
}

class PageContact {
    constructor() {
      //valores por defectos
      appFn.navLinkActive('#contact')
      appFn.consoleAlert()
    } 
} 

window.PageAbout = PageAbout
window.PageProduct = PageProduct
window.PageMarca = PageMarca
window.PageDistributor = PageDistributor
window.PageHelp = PageHelp
window.PageContact = PageContact
