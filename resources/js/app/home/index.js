import  appFn from './../../funtions'
import  shopping from './../../funtions/shopping'
import Swal from 'sweetalert2';

class HomeIndex {
    constructor(){
        appFn.consoleAlert()
        this.event()
    }
    event(){
        $(document).on('click', 'button.add-product',(e)=>{
          let idSlected = $(e.currentTarget).data('id')
          let title = document.getElementById(`title-product-${idSlected}`).innerHTML          
          let price = document.getElementById(`price-product-${idSlected}`).innerHTML    
          let amount = document.getElementById(`amount-product-input-${idSlected}`).value
          let carrito = {
            id: idSlected,
            title,
            price: parseFloat(price),
            total: parseFloat(price) * parseInt(amount) ,
            amount: parseInt(amount)
          } 
          shopping.add(carrito)
          document.getElementById(`amount-product-input-${idSlected}`).value = 1
        })
      } 

}

window.HomeIndex = HomeIndex