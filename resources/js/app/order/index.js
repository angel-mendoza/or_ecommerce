import  appFn from './../../funtions'
//
class OrderPublicIndex {
  constructor(){
    appFn.consoleAlert()
    appFn.initializeDataTable()
    this.event()
  }
  event(){
    $(document).on('click', 'button.deleted-order',(e)=>{
      let idSlected = $(e.currentTarget).data('id')
      let url = `ordenes/${idSlected}/`
      //let msg = 'Al borrar esta producto se borrara tambien de todas las ordenes que posean dicho producto.'
      let element = 'La orden'
      appFn.deletedSA(idSlected, url, element)
    })
  } 
}

class OrderIndex {
  constructor(){
    appFn.consoleAlert()
    appFn.initializeDataTable()
  }
}

class PayOrder{
  constructor(){
    appFn.consoleAlert()
  }  
}

class ConfirmPayment {
  constructor(){
    appFn.initializeselect2() 
    appFn.consoleAlert()
  }    
}

window.OrderPublicIndex = OrderPublicIndex
window.PayOrder = PayOrder
window.OrderIndex = OrderIndex
window.ConfirmPayment = ConfirmPayment