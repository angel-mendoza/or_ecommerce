import  appFn from './../../funtions'

class ProductIndex {
  constructor(){
    appFn.consoleAlert()
    appFn.initializeDataTable()
    this.event()
  }
  event(){
    $(document).on('click', 'button.deleted',(e)=>{
      let idSlected = $(e.currentTarget).data('id')
      let url = `productos/${idSlected}/`
      let msg = 'Al borrar esta producto se borrara tambien de todas las ordenes que posean dicho producto.'
      let element = 'El producto'
      appFn.deletedSA(idSlected, url, element, msg)
    })
  } 
}

class ProductNew {
    constructor(){
      appFn.consoleAlert()
      appFn.initializeselect2() 
      this.event()       
    }
    event(){
      $(document).on('change', 'select#category',(e)=>{
        appFn.fillSelectSubTag('#sub-category', $('#category').val())
      })
    }     
}

class ProductShow {
  constructor(){
    appFn.consoleAlert()
  }
}

class ProductEdit {
  constructor(){
    appFn.consoleAlert()
    appFn.initializeselect2() 
    this.event()       
  }
  event(){
    $(document).on('change', 'select#category',(e)=>{
      appFn.fillSelectSubTag('#sub-category', $('#category').val())
    })
  }       
}

window.ProductEdit = ProductEdit
window.ProductIndex = ProductIndex
window.ProductShow = ProductShow
window.ProductNew = ProductNew
