<div class="form-row">
    <div class="form-group col-md-6 col-sm-12">
        <label for="name">Nombre del Producto:</label>
        <input type="text" 
            @if (isset($product->name))
                value="{{ $product->name }}" 
            @else
                value="{{ old('name') ? old('name') : '' }}"    
            @endif
            placeholder="Ejemplo: Lavadora" 
            class="form-control @error('name') is-invalid @enderror" id="name" name="name"
        >
        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror  
    </div>
    <div class="form-group col-md-6 col-sm-12">
        <label for="price">Precio del Producto:</label>
        <input type="text"
            @if (isset($product->price))
                value="{{ $product->price }}" 
            @else
                value="{{ old('price') ? old('price') : '' }}"  
            @endif        
            placeholder="Ejemplo: 150.00"  class="form-control @error('price') is-invalid @enderror" id="price" name="price">
        <small id="priceHelp" class="form-text text-muted">Ingrese solo numeros, sin el simbolo de $.</small>
        @error('price')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror  
    </div>
</div>
<div class="form-group">
    <label for="description">Descripcion del Producto:</label>
    <input type="text" 
        placeholder="Ejemplo: Lavadora blanca de ultima generacion." 
        @if (isset($product->description))
            value="{{ $product->description }}" 
        @else
            value="{{ old('description') ? old('description') : '' }}" 
        @endif        
        class="form-control @error('description') is-invalid @enderror" id="description" name="description">
    @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror  
</div>

<div class="form-row">
    <div class="form-group col-md-6 col-sm-12">
        <label for="category">Categaria del Producto:</label>
        <select name="category" id="category" class="custom-select select2 @error('category') is-invalid @enderror">
            <option value="" selected>Seleccione una categoria</option>
            @foreach ($tags as $tag)
                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
            @endforeach
        </select>
        @error('category')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror   
    </div>
    <div class="form-group col-md-6 col-sm-12">
        <label for="sub-category">Sub-categaria del Producto:</label>
        <select name="sub-category" id="sub-category" class="custom-select select2 @error('sub-category') is-invalid @enderror">
            <option value="" selected>Elija una categoria primero</option>
        </select>
        @error('sub-category')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror   
    </div>    
</div>

<div class="form-group">
    <label for="photo">Imagen del producto</label>
    <input type="file" 
        @if (isset($product->file))
        value="{{ $product->file }}" 
    @else
        value="{{ old('photo') ? old('photo') : '' }}" 
    @endif
    aria-describedby="fileHelp" class="form-control-file @error('photo') is-invalid @enderror" id="photo" name="photo">     
    @error('photo')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror   
</div>
<button type="submit" class="btn btn-success btn-lg mb-2"><i class="far fa-save"></i> Guardar</button>

