@extends('layouts.dashboard')
@section('title')
Editar producto
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <h1 class="font-title d-inline-block">Editar producto</h1>
            <a class="btn btn-primary float-right"  data-toggle="tooltip" data-placement="bottom" title="Volver" href="{{ url()->previous() }}" role="button"><i class="fas fa-undo-alt"></i> Volver</a>
        </div>
        <div class="col-sm-10 offset-sm-1 mt-4">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('product-update', $product->id) }}" enctype="multipart/form-data">
                        @csrf					
                        @method('PUT')
                        @include('product.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    var self = new ProductEdit();       
</script>
@endpush
