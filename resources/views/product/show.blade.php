@extends('layouts.dashboard')
@section('title')
Detalle del producto
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <h1 class="font-title d-inline-block">Detalle de producto</h1>
            <a class="btn btn-primary float-right"  data-toggle="tooltip" data-placement="bottom" title="Volver" href="{{ url()->previous() }}" role="button"><i class="fas fa-undo-alt"></i> Volver</a>
        </div>
        <div class="col-sm-10 offset-sm-1 mt-4">

            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="{{ asset('images') }}/{{ $product->file }}" class="card-img custom-img-card" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->name }}</h5>
                            <p class="card-text">Descripcion: {{ $product->description }}</p>
                            <p class="card-text">Precio: {{ $product->price }}$</p>
                            <p class="card-text">Categoria: <i class="fa {{ $product->tag->icon }}"></i> {{ $product->tag->name }}</p>
                            <p class="card-text">Sub-categoria: {{ $product->sub_tag->name }}</p>
                        </div>
                    </div>
                </div>
            </div>            
            
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new ProductShow();
</script>
@endpush

