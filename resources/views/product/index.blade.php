@extends('layouts.dashboard')
@section('title')
Productos
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <h1 class="font-title d-inline-block">Productos</h1>
            <a class="btn btn-success float-right"  data-toggle="tooltip" data-placement="bottom" title="Crear un nuevo producto" href="{{ route('product-new')}}" role="button"><i class="fas fa-plus"></i> Nuevo producto</a>
        </div>
        <div class="col-sm-10 offset-sm-1 mt-4">
            <div class="table-responsive">



                <table id="dataTable" class="table table-striped table-bordered table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Categoria</th>
                            <th>Sub-categoria</th>
                            <th>Precio</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $key => $product)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $product->name }}</td>
                                <td><i class="fa {{ $product->tag->icon }}"></i> {{ $product->tag->name }}</td>
                                <td>{{ $product->sub_tag->name }}</td>
                                <td>{{ $product->price }}$</td>
                                <td class="text-right">
                                    <span data-toggle="tooltip" data-placement="bottom" title="Detalle del Producto">
                                        <a class="btn btn-outline-success" href="{{ route('product-show', $product->id) }}" role="button"><i class="far fa-eye"></i></a>
                                    </span>
                                    <span data-toggle="tooltip" data-placement="bottom" title="Editar Producto">
                                        <a class="btn btn-outline-warning" href="{{ route('product-edit', $product->id) }}" role="button"><i class="far fa-edit"></i></a>
                                    </span>                                    															
                                    <button class="btn btn-outline-danger deleted" data-id="{{ $product->id }}" data-toggle="tooltip" data-placement="bottom" title="Eliminar producto"><i class="far fa-trash-alt"></i></button>
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
          
                    <tfoot class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Categoria</th>
                            <th>Sub-categoria</th>
                            <th>Precio</th>
                            <th>Acciones</th>
                        </tr>
                    </tfoot>
                </table>
            </div> 
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new ProductIndex();
var alert = '{{ Session::has('alert') }}';
if(alert){
    Swal.fire({
        title: '{{ Session::get('alert') }}',
        type: 'success',
    })
}
</script>
@endpush
