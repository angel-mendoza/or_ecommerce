@extends('layouts.app')
@section('title')
productos
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        @foreach ($tags as $key => $tag)
            <div class="col-3">
                <div class="card">
                    <div class="card-body">
                        <h5 data-toggle="collapse" 
                            href="#collapseSubTag-{{ $key }}" 
                            role="button" 
                            aria-expanded="false" 
                            aria-controls="collapseSubTag-{{ $key }}" 
                            class="card-title cursor-p mb-0"><i class="fa {{ $tag->icon }}"></i> {{ $tag->name }} <i class="fas fa-sort-down float-right"></i></h5>
                        <div class="collapse mt-1" id="collapseSubTag-{{ $key }}">
                            <div class="card card-body">
                                @foreach ($tag->sub_tag as $k => $subTag)
                                    <a class="btn btn-Light text-left" role="button" href="{{ route('product-tag', $subTag->id)}}">{{ $subTag->name }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new PageProduct();
var alert = '{{ Session::has('alert') }}';
if(alert){
    Swal.fire({
        title: '{{ Session::get('alert') }}',
        type: 'success',
    })
}
</script>
@endpush
