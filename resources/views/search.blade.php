@extends('layouts.app')
@section('title')
Busqueda: "{{ $query }}"...
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <h1 class="font-title d-inline-block">Busqueda: "{{ $query }}"...</h1>
            <a class="btn btn-primary float-right"  data-toggle="tooltip" data-placement="bottom" title="Volver" href="{{ url()->previous() }}" role="button"><i class="fas fa-undo-alt"></i> Volver</a>
        </div>
        <div class="col-sm-10 offset-sm-1 mt-4">
            <div class="row">
                @if( count($products) > 0)
                    @foreach ($products  as $key => $product)
                        <div class="col-12 col-md-6 col-xl-3 mb-4">
                            <div class="card">
                                <img src="{{ asset('images') }}/{{ $product->file }}" class="card-img-top custom-img-card" alt="{{ $product->name }}">
                                <div class="card-body">
                                    <h5 id="title-product-{{ $product->id }}" class="card-title">{{ $product->name }}</h5>
                                    <h6 id="price-product-{{ $product->id }}" class="card-subtitle mb-2 text-success">{{ $product->price }}$</h6>
                                    <div class="footer-card-poduct">
                                        <span data-toggle="tooltip" data-placement="bottom" title="Detalle del Producto">
                                            <a class="btn btn-primary" href="{{ route('home-show-product', $product->id) }}" role="button">Detalle</a>
                                        </span>
                                        @guest
                                            <span data-toggle="tooltip" data-placement="bottom" title="Agregar al carrito">
                                                <button class="btn btn-success" type="button" data-toggle="collapse" data-target="#collapseLogin-{{ $key }}" aria-expanded="false" aria-controls="collapseExample">
                                                    Agregar
                                                </button>
                                            </span> 
                                        @else
                                            <button class="btn btn-success add-product" data-id="{{ $product->id }}" type="button" data-toggle="tooltip" data-placement="bottom" title="Agregar al carrito">
                                                Agregar
                                            </button>                                
                                        @endguest
                                        <span data-toggle="tooltip" data-placement="bottom" title="Cantidad del producto">
                                            <input type="number" class="form-control" id="amount-product-input-{{ $product->id }}" value="1">                                    
                                        </span>    
                                    </div>
                                    <div class="collapse mt-1" id="collapseLogin-{{ $key }}">
                                        <p>Para poder agregar el producto al carrito de compra debes ingresar al sistema, ingresa <a href="{{ route('login') }}">aqui!</a></p>
                                    </div>                                
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else 
                    <div class="col-12">
                        <h5>La busqueda "{{ $query }}" no arrojo ningun resultado... intente con otra palabra.</h5>
                    </div>
                @endif
            </div>            
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new HomeIndex();
</script>
@endpush

