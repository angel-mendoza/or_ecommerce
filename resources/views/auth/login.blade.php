@extends('layouts.app2')
@section('title')
Ingresar
@endsection
@section('content')
<div class="layout-center">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-4 offset-md-4">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title font-title mb-3">Ingresar</h3>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <input id="email" type="email" placeholder="Correo Electronico" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="form-group">
                                <input id="password" type="password" placeholder="Contraseña" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary btn-block btn-lg">
                                    Ingresar
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 offset-md-4 mt-2">
                <div class="card">
                    <div class="card-body">
                        <a class="nav-link" href="{{ route('register') }}">¿No tienes una cuenta? reguistrate aqui.</a>
                    </div>
                </div>                
            </div>
        </div>
    </div>

</div>        
@endsection
@push('scripts')
    <script>
        var self = new LoginIndex();
    </script>
@endpush





