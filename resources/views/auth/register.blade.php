@extends('layouts.app2')
@section('title')
Registrarte
@endsection
@section('content')

<div class="layout-center">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name">Nombre</label>        
                                        <input id="name" type="text" placeholder="Ejemplo: Jose Mendoza" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
        
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
        
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="dni">Cedula</label>        
                                        <input id="dni" type="text" placeholder="Ejemplo: 001002003004" class="form-control @error('dni') is-invalid @enderror" name="dni" value="{{ old('dni') }}" required autocomplete="dni">
        
                                        @error('dni')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">        
                                        <label for="address">Direccion</label>        
                                        <input id="address" placeholder="Ejemplo: El Oro, Machala ciudadela los girasoles bloque 8 departamento 101" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address">
                                        <small id="emailHelp" class="form-text text-muted">Sea muy especifico con su direccion, puesto que esta sera donde se enviaran sus pedidos.</small>
                                        @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">        
                                        <label for="phone">Telefono</label>        
                                        <input id="phone" type="text"placeholder="Ejemplo: 099001002003" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">
        
                                        @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="email">Correo Electronico</label>        
                                        <input id="email" type="email" placeholder="Ejemplo: Example@gemail.com" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>           
                                </div>                            
                            </div>                            
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="password">Contraseña</label>        
                                        <input id="password" placeholder="Ejemplo: cualquier contraseña servira :)" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
        
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group ">
                                        <label for="password-confirm">Confirmar contraseña</label>        
                                        <input id="password-confirm" placeholder="Repetir la contraseña" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block btn-lg">Ingresar</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 offset-md-3 mt-2">
                <div class="card">
                    <div class="card-body">
                        <a class="nav-link" href="{{ route('login') }}">¿Ya tienes una cuenta? ingresa aqui.</a>
                    </div>
                </div>                
            </div>
        </div>
    </div>

</div>        
@endsection
@push('scripts')
    <script>
        var self = new RegisterIndex();
    </script>
@endpush