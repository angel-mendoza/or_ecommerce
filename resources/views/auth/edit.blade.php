@extends('layouts.app')
@section('title')
Inf. del usuario
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <h1 class="font-title d-inline-block">Editar informacion personal.</h1>
            <a class="btn btn-primary float-right"  data-toggle="tooltip" data-placement="bottom" title="Volver" href="{{ url()->previous() }}" role="button"><i class="fas fa-undo-alt"></i> Volver</a>
        </div>
        <div class="col-sm-10 offset-sm-1 mt-4">
            @includeIf('layouts.alert', [
                        'strong' => "Advertecia." , 
                        'msg' => 'esta informacion se utilizara para ponerce en contacto con usted, para tramitar pagos o envios de productos.', 
                        'type' => 'warning'])
            <div class="card mb-3">
                <div class="card-body">
                    <form method="POST" action="{{ route('user-update', $user->id) }}">
                        @csrf					
                        @method('PUT')


<div class="form-row">
    <div class="col-sm-12 col-md-6">
        <label for="name">Nombre:</label>
        <input type="text" 
        @if (isset($user->name))
            value="{{ $user->name }}" 
        @else
            value="{{ old('name') ? old('name') : '' }}" 
        @endif        
        class="form-control mb-3 @error('name') is-invalid @enderror" id="name" name="name">
        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror   
    </div>
    <div class="col-sm-12 col-md-6">
        <label for="dni">Cedula:</label>
        <input type="text" 
        @if (isset($user->dni))
            value="{{ $user->dni }}" 
        @else
            value="{{ old('dni') ? old('dni') : '' }}" 
        @endif        
        class="form-control mb-3 @error('dni') is-invalid @enderror" id="dni" name="dni">
        @error('dni')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror 
    </div>
</div>
<div class="form-row">
    <div class="col-sm-12 col-md-6">
        <label for="address">Direccion:</label>
        <input type="text" 
        @if (isset($user->address))
            value="{{ $user->address }}" 
        @else
            value="{{ old('address') ? old('address') : '' }}" 
        @endif        
        class="form-control mb-3 @error('address') is-invalid @enderror" id="address" name="address">
        @error('address')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror 
    </div>
    <div class="col-sm-12 col-md-6">
        <label for="phone">Direccion:</label>
        <input type="text" 
        @if (isset($user->phone))
            value="{{ $user->phone }}" 
        @else
            value="{{ old('phone') ? old('phone') : '' }}" 
        @endif        
        class="form-control mb-3 @error('phone') is-invalid @enderror" id="phone" name="phone">
        @error('phone')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror 
    </div>
</div>

                     
                        <button type="submit" class="btn btn-success btn-lg mb-2"><i class="far fa-save"></i> Guardar</button>
    
                    </form>
                </div>
            </div>            
            
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new EditUser();
</script>
@endpush

