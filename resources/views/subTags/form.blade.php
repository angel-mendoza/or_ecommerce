<div class="row mb-3">
<div class="col-12 col-md-6">
    <label for="name" class="mr-1">Nombre de la Categoria:</label>
    <input type="text"
        @if (isset($subTag->name))
            value="{{ $subTag->name }}" 
        @else
            value="{{ old('name') ? old('name') : '' }}" 
        @endif  
        placeholder="Ejemplo: Pantallas" class="form-control mr-1 @error('name') is-invalid @enderror" id="name" name="name">
    @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror                    
</div>
<div class="col-12 col-md-6">
    <label for="category">Categaria:</label>
    <select name="category" id="category" class="custom-select select2 @error('category') is-invalid @enderror">
        <option value="">Seleccione una categoria</option>
        @foreach ($tags as $tag)    
            <option value=" {{ $tag->id }} ">{{ $tag->name }}</option>
        @endforeach        
    </select>
    @error('category')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror                    
</div>
<div class="col mt-3">
    <button type="submit" class="btn btn-success btn-lg mb-2"><i class="far fa-save"></i> Guardar</button>
</div>