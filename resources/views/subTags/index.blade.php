@extends('layouts.dashboard')
@section('title')
Sub-Categorias
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <h1 class="font-title d-inline-block">Sub-Categorias</h1>
            <a class="btn btn-success float-right"  data-toggle="tooltip" data-placement="bottom" title="Crear una nueva sub-categoria" href="{{ route('sub-tag-new')}}" role="button"><i class="fas fa-plus"></i> Nueva Sub-categoria</a>
        </div>
        <div class="col-sm-10 offset-sm-1 mt-4">
            <div class="table-responsive">



                <table id="dataTable" class="table table-striped table-bordered table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Categoria</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($subTags as $key => $subTag)
                            <tr>
                              <td>{{ $key+1 }}</td>
                              <td>{{ $subTag->name }}</td>
                              <td><i class="fa {{ $subTag->tag->icon }}"></i> {{ $subTag->tag->name }}</td>

                              <td class="text-right">
                                    <span data-toggle="tooltip" data-placement="bottom" title="Editar categoria">
                                        <a class="btn btn-outline-warning" href="{{ route('sub-tag-edit', $subTag->id) }}" role="button"><i class="far fa-edit"></i> Ediatr</a>
                                    </span>  
                                  <button class="btn btn-outline-danger deleted"  data-id="{{ $subTag->id}}" data-toggle="tooltip" data-placement="bottom" title="Eliminar Categoria"><i class="far fa-trash-alt"></i> Eliminar</button>
                              </td>
                            </tr>
                        @endforeach
                      </tbody>
          
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Categoria</th>
                            <th>Acciones</th>
                        </tr>
                    </tfoot>
                </table>
            </div> 
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new SubTagIndex();
var alert = '{{ Session::has('alert') }}';
if(alert){
    Swal.fire({
        title: '{{ Session::get('alert') }}',
        type: 'success',
    })
}
</script>
@endpush
