@extends('layouts.app')
@section('title')
Ordenes
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <h1 class="font-title d-inline-block">Ordenes</h1>
        </div>
        <div class="col-sm-10 offset-sm-1 mt-4">
            <div class="table-responsive">
                <table id="dataTable" class="table table-striped table-bordered table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Usuario</th>
                            <th>Total</th>
                            <th>Estado</th>
                            <th>Facha</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $key => $order)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $order->user->name }}</td>
                                <td>{{ $order->total }}$</td>
                                <td>{{ $order->state }}</td>
                                <td>{{ date_format($order->created_at, 'd/m/Y') }}</td>
                                <td class="text-right">
                                    <span data-toggle="tooltip" data-placement="bottom" title="Detalle del la orden">
                                        <a class="btn btn-outline-success" href="{{ route('detail-order-admin', $order->id) }}" role="button"><i class="far fa-eye"></i> Detalle</a>
                                    </span>
                                    @if($order->state != 'pedido recibido')
                                        <span data-toggle="tooltip" data-placement="bottom" title="Confirmar pago">
                                            <a class="btn btn-outline-warning" href="{{ route('confirm-payment', $order->id) }}" role="button"><i class="far fa-edit"></i> Confirmar</a>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
          
                    <tfoot class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Usuario</th>
                            <th>Total</th>
                            <th>Estado</th>
                            <th>Fecha</th>
                            <th>Acciones</th>
                        </tr>
                    </tfoot>
                </table>
            </div> 
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new OrderIndex();
var alert = '{{ Session::has('alert') }}';
if(alert){
    Swal.fire({
        title: '{{ Session::get('alert') }}',
        type: 'success',
    })
}
</script>
@endpush
