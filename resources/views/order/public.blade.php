@extends('layouts.app')
@section('title')
Ordenes realizadas
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <h1 class="font-title d-inline-block">Ordenes</h1>
        </div>
        <div class="col-sm-10 offset-sm-1">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseHowToPay" aria-expanded="false" aria-controls="collapseHowToPay">
                <i class="far fa-question-circle"></i> Como pagar 
            </button>
            <div class="collapse mt-3" id="collapseHowToPay">
              <div class="card card-body">
                <p class="mb-2">Si usted realizo un pedido, debe presionar el botón de pago, 
                    una vez dentro de la sección de pago debe introducir el código de transacción bancaria. 
                    Cuando la transacción sea confirmada nuestros operadores se pondrán en contacto con usted.</p>
                @include('layouts.info-bancaria')

              </div>
            </div>
        </div>        
        <div class="col-sm-10 offset-sm-1 mt-4">
            @foreach ($orders as $key => $order)
                <?php $key++; ?>
                @if ($order->state === 'pedido recibido')
                    @includeIf('layouts.alert', [
                        'strong' => "Orden numero $key" , 
                        'msg' => 'debe introducir el código de transacción bancaria. Cuando la transacción sea confirmada nuestros operadores se pondrán en contacto con usted.', 
                        'type' => 'warning'])
                @elseif ($order->state === 'transacción realizada')
                    @includeIf('layouts.alert', [
                        'strong' => "Orden numero $key" , 
                        'msg' => 'numero de documento enviado. Cuando la transacción sea confirmada nuestros operadores se pondrán en contacto con usted.', 
                        'type' => 'success'])
                @elseif ($order->state === 'transacción rechazada')
                    @includeIf('layouts.alert', [
                        'strong' => "Orden numero $key" , 
                        'msg' => 'transacción rechazada. La transacción no pudo se comprovada, confirme el numero de documento.', 
                        'type' => 'danger'])    
                @elseif ($order->state === 'pedido confirmado')
                    @includeIf('layouts.alert', [
                        'strong' => "Orden numero $key" , 
                        'msg' => 'pedido confirmado. Nuestros operadores se pondrán en contacto con usted.', 
                        'type' => 'info'])  
               @elseif ($order->state === 'pedido cerrado')
                    @includeIf('layouts.alert', [
                        'strong' => "Orden numero $key" , 
                        'msg' => 'pedido cerrado. si lo desea puede eliminar la orden.', 
                        'type' => 'info'])                                                                         
                @endif                
            @endforeach
            @if (count($orders) > 0)
                <div class="table-responsive">
                    <table id="dataTable" class="table table-striped table-bordered table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Total</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $key => $order)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $order->total }}$</td>
                                    <td>{{ $order->state }}</td>
                                    <td class="text-right">
                                        <span data-toggle="tooltip" data-placement="bottom" title="Detalle del la orden">
                                            <a class="btn btn-outline-primary" href="{{ route('order-public-detail', $order->id) }}" role="button"><i class="far fa-eye"></i> Detalle</a>
                                        </span>
                                        @if ($order->state == 'pedido recibido' || $order->state == 'transacción rechazada')
                                            <span data-toggle="tooltip" data-placement="bottom" title="Pagar la orden">
                                                <a class="btn btn-outline-success" href="{{ route('pay-order', $order->id) }}" role="button"><i class="fas fa-dollar-sign"></i> Pagar</a>
                                            </span> 
                                        @endif
                                        @if ($order->state === 'pedido cerrado')
                                            <button class="btn btn-outline-danger deleted-order" data-id="{{ $order->id }}" data-toggle="tooltip" data-placement="bottom" title="Eliminar orden"><i class="far fa-trash-alt"></i> Eliminar</button>                                                                    
                                        @endif                                     
                                    </td>
                                </tr>
                            @endforeach
                          </tbody>
              
                        <tfoot class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Total</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div> 
            @else
                <div class="card">
                    <div class="card-body text-center">
                        <i class="far fa-folder-open fa-3x"></i>
                        <h3 class="font-title">No hay ordenes registradas.</h3>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new OrderPublicIndex();
var alert = '{{ Session::has('alert') }}';
if(alert){
    Swal.fire({
        title: '{{ Session::get('alert') }}',
        type: 'success',
    })
}
</script>
@endpush
