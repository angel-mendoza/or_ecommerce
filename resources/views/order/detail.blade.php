@extends('layouts.app')
@section('title')
Detalle de orden
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <h1 class="font-title d-inline-block">Detalle de la orden</h1>
            <a class="btn btn-primary float-right"  data-toggle="tooltip" data-placement="bottom" title="Volver" href="{{ url()->previous() }}" role="button"><i class="fas fa-undo-alt"></i> Volver</a>
        </div>
        <div class="col-sm-10 offset-sm-1 mt-4">

            <div class="card mb-2">
                <div class="card-body">
                    <h5 class="card-title">Datos del usuario:</h5>
                    <p class="card-text">Nombre: {{ $order->user->name }}.</p>
                    <p class="card-text">Cedula: {{ $order->user->dni }}.</p>
                    <p class="card-text">Telefono de contacto: {{ $order->user->phone }}.</p>
                    <p class="card-text">Email de contacto: {{ $order->user->email }}.</p>
                    <p class="card-text">Direccion de envio: {{ $order->user->address }}.</p>
                </div>
            </div>            
            <div class="card mb-2">
                <div class="card-body">
                    <h5 class="card-title">Datos de la orden:</h5>
                    <p class="card-text">Estado: {{ $order->state }}.</p>
                    <p class="card-text">Numero de documento: {{ $order->transaction }}.</p>
                    <p class="card-text">Fecha en que se realizo: {{ date_format($order->created_at, 'd/m/Y') }}.</p>
                    <p class="card-text">Hora en que se realizo: {{ date_format($order->created_at, 'g:i A') }}.</p>
                    <p class="card-text">total a pagar: {{ $order->total }}$</p>
                </div>
            </div>
            <div class="card mb-2">
                <div class="card-body">
                    <h5 class="card-title mb-3">Productos ordenados:</h5>
                    <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead class="thead-dark">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Producto</th>
                                        <th class="text-center">Precio</th>
                                        <th class="text-center">Cantidad</th>
                                        <th class="text-center">Total</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($order->OrderDetail as $key => $detail)
                                        <tr>
                                            <td class="text-center">{{ $key+1 }}</td>
                                            <td class="text-center">{{ $detail->product->name }}</td>
                                            <td class="text-center">{{ $detail->product->price }}$</td>
                                            <td class="text-center">{{ $detail->amount }} unidades</td>
                                            <td class="text-center">{{ $detail->amount * $detail->product->price}}</td>
                                        </tr>
                                    @endforeach
                                        <tr>
                                            <td colspan="3"></td>
                                            <td class="text-center"><strong>Total a pagar:</strong></td>
                                            <td class="text-center"><strong>{{ $order->total }}$</strong></td>
                                        </tr>
                                  </tbody>
                            </table>
                        </div> 
                </div>
            </div>             
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new ProductShow();
</script>
@endpush

