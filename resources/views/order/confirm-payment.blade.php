@extends('layouts.app')
@section('title')
Confirmar pago
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <h1 class="font-title d-inline-block">Confirmar pago</h1>
            <a class="btn btn-primary float-right"  data-toggle="tooltip" data-placement="bottom" title="Volver" href="{{ url()->previous() }}" role="button"><i class="fas fa-undo-alt"></i> Volver</a>
        </div>
        <div class="col-sm-6 offset-sm-3 mt-4">

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Codigo: {{ $order->transaction ? $order->transaction : 'aun no realizan el pago.' }}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Estado del pedido: {{ $order->state }}</h6>
                    <p class="card-text">
                        Introduzca el código de la transferencia bancaria y presione procesar, 
                        cuando la transacción sea confirmada nuestro personal se pondrán en contacto con ustedes.                       
                    </p>
                    <form method="POST" action="{{ route('order-changeState', $order->id) }}">
                        @csrf					
                        @method('PUT')
               
                        <div class="form-group">
                            <label for="state">Categaria del Producto:</label>
                            <select name="state" id="state" class="custom-select select2 @error('state') is-invalid @enderror">
                                <option value="" selected>Seleccione una categoria</option>
                                <option value="pedido recibido">Pedido recibido</option>
                                <option value="transacción realizada">Transacción realizada</option>
                                <option value="transacción rechazada">Transacción rechazada</option>
                                <option value="pedido confirmado">Pedido confirmado</option>
                                <option value="pedido cerrado">Pedido cerrado</option>
                            </select>
                            @error('state')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror  

                        </div>                        
                        <button type="submit" class="btn btn-success btn-lg mb-2"><i class="far fa-save"></i> Guardar</button>

                    </form>
                </div>
              </div>

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new ConfirmPayment();
var alert = '{{ Session::has('alert') }}';
if(alert){
    Swal.fire({
        title: '{{ Session::get('alert') }}',
        type: 'success',
    })
}
</script>
@endpush