@extends('layouts.app')
@section('title')
Pagar orden
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <h1 class="font-title d-inline-block">Pagar pedido</h1>
            <a class="btn btn-primary float-right"  data-toggle="tooltip" data-placement="bottom" title="Volver" href="{{ url()->previous() }}" role="button"><i class="fas fa-undo-alt"></i> Volver</a>
        </div>
        <div class="col-sm-6 offset-sm-3 mt-4">

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Forma de pago</h5>
                    <p class="card-text">
                        Introduzca el código de la transferencia bancaria y presione procesar, 
                        cuando la transacción sea confirmada nuestro personal se pondrán en contacto con ustedes.                       
                    </p>
                    <form method="POST" action="{{ route('payment', $order->id) }}">
                        @csrf					
                        @method('PUT')
               
                        <div class="form-group">
                            <label for="transaction">Código de la transferencia bancaria :</label>
                            <input type="text" value="{{ old('transaction') ? old('transaction') : '' }}"    
                                placeholder="Ejemplo: 00000000" 
                                class="form-control @error('transaction') is-invalid @enderror" id="transaction" name="transaction">
                            <small id="emailHelp" class="form-text text-muted">Introduzca solo números, sin espacios y sin guiones.</small>
                            
                            @error('transaction')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror  

                        </div>                        
                        <button type="submit" class="btn btn-success btn-lg mb-2"><i class="far fa-save"></i> Guardar</button>

                    </form>
                </div>
              </div>

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new PayOrder();
var alert = '{{ Session::has('alert') }}';
if(alert){
    Swal.fire({
        title: '{{ Session::get('alert') }}',
        type: 'success',
    })
}
</script>
@endpush
