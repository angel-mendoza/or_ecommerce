<div class="input-group">
    <input type="text" id="input-search-product" placeholder="Buscar..." class="form-control">
    <div class="input-group-append">
        <button class="btn btn-danger" id="btn-search-product"><i class="fas fa-search-dollar"></i> Buscar</button>
    </div>
</div>