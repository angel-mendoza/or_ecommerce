<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top p-3">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button type="button"  class="navbar-toggler link-cart"><i class="fas fa-shopping-cart"></i><span id="amount-shopping-cart" class="badge badge-pill badge-success amount-shopping-cart"></span></button> 
    <ul class="nav navbar-nav navbar-center d-none d-lg-block d-xl-none d-xl-block">
        @include('layouts.buscador')
    </ul>    
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="{{ url('/') }}">Hidden branda</a>
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" id="products" href="{{ route('products') }}">Porductos</a>
            </li>
            @guest  
                <li class="nav-item">
                    <a class="nav-link" id="about" href="{{ route('about') }}">Quines somos?</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="marca" href="{{ route('marca') }}">Marca</a>
                </li>                
            @endguest
        </ul>
        <ul class="navbar-nav mt-2 mt-lg-0">
            @guest
                <li class="nav-item">
                    <a class="nav-link" id="distributor" href="{{ route('distributor') }}">Como ser distribuidor?</a>
                </li>   
                <li class="nav-item">
                    <a class="nav-link" id="help" href="{{ route('help') }}">ayuda</a>
                </li>   
                <li class="nav-item">
                    <a class="nav-link" id="contact" href="{{ route('contact') }}">Contacto</a>
                </li>                       

                <li class="nav-item">
                    <a class="nav-link" id="login" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" id="register" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item">
                    <button type="button" class="btn btn-link nav-link" data-toggle="modal" data-target="#modal-como-pagar">Como pagar?</button>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="order" href="{{ route('order-index') }}">Ordenes <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item d-none d-lg-block d-xl-none d-xl-block">              
                    <button type="button" class="btn btn-link nav-link link-cart"><i class="fas fa-shopping-cart"></i><span id="amount-total" class="badge badge-pill badge-success amount-shopping-cart"></span></button>
                </li>                             
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fas fa-user-circle fa-user-icon"></i> <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        @if (Auth::user()->role == "admin")
                            <a id="tag" class="dropdown-item" href="{{ route('tag-index') }}">Categorias<span class="sr-only">(current)</span></a>
                            <a id="tag" class="dropdown-item" href="{{ route('sub-tag-index') }}">Sub-categorias<span class="sr-only">(current)</span></a>
                            <a id="product" class="dropdown-item" href="{{ route('product-index') }}">Productos<span class="sr-only">(current)</span></a>
                            <div role="separator" class="dropdown-divider"></div>
                            @endif                        
                            <a id="tag" class="dropdown-item" href="{{ route('edit-user') }}">editar inf.<span class="sr-only">(current)</span></a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest                    
        </ul>        
    </div>
</nav>
