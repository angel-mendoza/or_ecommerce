<!-- Modal -->
<div class="modal fade" id="modal-shopping-cart" tabindex="-1" role="dialog" aria-labelledby="modal-shopping-cart-Title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-shopping-cart-Title"><i class="fas fa-shopping-cart text-success"></i> Carrito de compra</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <table id="table-product" class="table table-hover">
              <thead class="thead-dark">
                <tr>
                  <th scope="col"></th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Cantidad</th>
                  <th scope="col">Precio</th>
                  <th scope="col">total</th>
                </tr>
              </thead>
              <tbody id="body-shopping-cart">
      
              </tbody>
          </table>
          <div id="loading-body" style="display: none;">
            <div class='loader'>Loading...</div>
            <h2 class='text-center'>Procesando pedido</h2>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="delete-order" class="btn btn-secondary" ><i class="fas fa-times"></i> Cancelar pedido</button>
          <button type="button" id="process-order" class="btn btn-success"><i class="fas fa-dollar-sign"></i> Procesar compra</button>
        </div>
      </div>
    </div>
  </div>