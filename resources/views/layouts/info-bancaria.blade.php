<div class="table-responsive">
    <table class="table table-hover">
        <thead class="thead-dark">
            <tr>
                <th class="text-center" scope="col">Informacion Bancaria</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><strong>Banco pichincha</strong></td>
            </tr>
            <tr>
                <td><strong>Tipo de cuenta:</strong> Corriente</td>
            </tr>
            <tr>
                <td><strong>Numero de cuenta:</strong> 2100147585</td>
            </tr>
            <tr>
                <td><strong>Numero de RUC:</strong> 0504056151001</td>
            </tr>
            <tr>
                <td><strong>Propietario de la cuenta:</strong> José Luis Oña Riera</td>
            </tr>
            <tr>
                <td><strong>Direccion:</strong>  Av. 19 de Mayo y Eugenio Espejo.</td>
            </tr>
        </tbody>
    </table>
</div>