<!-- Modal -->
<div class="modal fade" id="modal-como-pagar" tabindex="-1" role="dialog" aria-labelledby="modal-como-pagar-Title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-como-pagar-Title"><i class="fas fa-question-circle text-success"></i> Como pagar?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p class="mb-3">
                al realizar el pedido solo tiene que realizar la transferencia bancaria con el total de su pedido y listo. Nuestros datos se encuentran a continuacion.
            </p>
            @include('layouts.info-bancaria')
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times" aria-hidden="true"></i> Cerrar</button>
        </div>
      </div>
    </div>
  </div>