<div class="row">

    <div class="col-12 col-md-6">
        <label for="name" class="mr-1">Nombre de la Categoria:</label>
        <input type="text"
        @if (isset($tag->name))
        value="{{ $tag->name }}" 
        @else
        value="{{ old('name') ? old('name') : '' }}" 
        @endif  
        placeholder="Ejemplo: Pantallas" class="form-control mr-1 @error('name') is-invalid @enderror" id="name" name="name">
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror                    
    </div>
    <div class="col-12 col-md-6">
            <label for="icon" class="mr-1">Icono:</label>
            <input type="text"
            @if (isset($tag->icon))
            value="{{ $tag->icon }}" 
            @else
            value="{{ old('icon') ? old('icon') : '' }}" 
            @endif  
            placeholder="Ejemplo: fa-glass" class="form-control mr-1 @error('icon') is-invalid @enderror" id="icon" name="icon">
            <small id="iconHelp" class="form-text text-muted">Debe copiar solo el codigo de icono.</small>
            @error('icon')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror                    
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-success btn-lg"><i class="far fa-save"></i> Guardar</button>
    </div>
    <div class="col-12">
        @include('layouts.list-icon')
        
    </div>
</div>