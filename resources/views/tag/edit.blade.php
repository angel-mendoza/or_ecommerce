@extends('layouts.dashboard')
@section('title')
Editar categoria
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <h1 class="font-title d-inline-block">Editar categoria</h1>
            <a class="btn btn-primary float-right"  data-toggle="tooltip" data-placement="bottom" title="Volver" href="{{ url()->previous() }}" role="button"><i class="fas fa-undo-alt"></i> Volver</a>
        </div>
        <div class="col-sm-10 offset-sm-1 mt-4">
            <form method="POST" action="{{ route('tag-update', $tag->id) }}">
                @csrf
                @method('PUT')
                @include('tag.form')
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
var self = new TagEdit();
</script>
@endpush
