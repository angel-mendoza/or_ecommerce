@extends('layouts.app')
@section('title')
Contactanos
@endsection
@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-sm-10 offset-sm-1">
                <h1 class="font-title d-inline-block">Contactanos</h1>
            </div>
            <div class="col-sm-10 offset-sm-1 mt-4">
 
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
var self = new PageContact();
var alert = '{{ Session::has('alert') }}';
if(alert){
    Swal.fire({
        title: '{{ Session::get('alert') }}',
        type: 'success',
    })
}
</script>
@endpush
